//
//  SDKGO.h
//  SDKGO
//
//  Created by Prigent ROUDAUT on 15/11/2017.
//  Copyright © 2017 HighConnexion. All rights reserved.
//

#import <SDKGO/SDKGOAdsManagerDelegate.h>
#import <SDKGO/SDKGOConsentManagerDelegate.h>
#import <SDKGO/SDKGOConsent.h>

//! Project version number for SDKGO.
FOUNDATION_EXPORT double SDKGOVersionNumber;

//! Project version string for SDKGO.
FOUNDATION_EXPORT const unsigned char SDKGOVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SDKGO/PublicHeader.h>

@interface  SDKGO : NSObject

//FUNCTION FOR CONFIGURATION
+ (void) configure;

//FUNCTION FOR TRACKING MANAGEMENT
+ (void) startTracking;

//FUNCTION FOR CONSENT MANANGEMENT
+ (void) verifyConsent:(NSString*) type withDelegate:(id<SDKGOConsentManagerDelegate>) delegate withBlock:(void (^)(NSNumber * data)) onCompletion;
+ (void) getConsents:(void (^)(NSArray<SDKGOConsent*> * data))onCompletion;
+ (void) getConsent:(NSString*) type onCompletion:(void (^)(SDKGOConsent * consent))onCompletion;
+ (void) acceptConsent:(NSString*) type withStatus:(NSNumber*) status;

//FUNCTION FOR ADS MANANGEMENT
+ (void) manageAppEvent:(NSString*) name withInfo:(NSString*) info delegate:(id<SDKGOAdsManagerDelegate>) delegate;
+ (void) getAdData:(void (^)(NSDictionary * data))onCompletion;

//FUNCTION FOR UPDATE SDK IN BACKGROUND
+ (void)fetchInBackground:(void (^)(UIBackgroundFetchResult))completionHandler;

@end
