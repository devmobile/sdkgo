//
//  SDKGOConsentManagerDelegate.h
//  SDKGO
//
//  Created by Prigent ROUDAUT on 15/11/2017.
//  Copyright © 2017 HighConnexion. All rights reserved.
//

@import Foundation;
@import UIKit;

@protocol SDKGOConsentManagerDelegate <NSObject>
@required
- (UIViewController*) viewControllerForPopUp;
@end

