# com.tenth-avenue.go

[![CI Status](http://img.shields.io/travis/Prigent-HighConnexion/com.tenth-avenue.go.svg?style=flat)](https://travis-ci.org/Prigent-HighConnexion/com.tenth-avenue.go)
[![Version](https://img.shields.io/cocoapods/v/com.tenth-avenue.go.svg?style=flat)](http://cocoapods.org/pods/com.tenth-avenue.go)
[![License](https://img.shields.io/cocoapods/l/com.tenth-avenue.go.svg?style=flat)](http://cocoapods.org/pods/com.tenth-avenue.go)
[![Platform](https://img.shields.io/cocoapods/p/com.tenth-avenue.go.svg?style=flat)](http://cocoapods.org/pods/com.tenth-avenue.go)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

com.tenth-avenue.go is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'com.tenth-avenue.go'
```

## Author

Prigent-HighConnexion, xtra.creativity@gmail.com

## License

com.tenth-avenue.go is available under the MIT license. See the LICENSE file for more info.
